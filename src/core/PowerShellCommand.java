package core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class PowerShellCommand {

	String command;
	ArrayList<String> out = new ArrayList<String>();
	
	public PowerShellCommand(String cmd) {

  //String command = "powershell.exe  your command";
  //Getting the version
 	command = cmd;

	}
 
	public ArrayList<String> execute () throws IOException{
		// Executing the command
		Process powerShellProcess = Runtime.getRuntime().exec(command);
		// Getting the results
		powerShellProcess.getOutputStream().close();
		String line;
		System.out.println("Standard Output:");
		BufferedReader stdout = new BufferedReader(new InputStreamReader(powerShellProcess.getInputStream()));
	    while ((line = stdout.readLine()) != null) {
	    	System.out.println(line);
	    	out.add(line);
	    }
	    stdout.close();
	   	System.out.println("Standard Error:");
	    BufferedReader stderr = new BufferedReader(new InputStreamReader(powerShellProcess.getErrorStream()));
	    while ((line = stderr.readLine()) != null) {
	    	System.out.println(line);
	    }
	    stderr.close();
	    System.out.println("Done");
	    return out;
	}

}