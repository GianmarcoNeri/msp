package core;

import java.util.HashMap;


public abstract class ConstructedDeckList extends AbstractDeckList {
	public static final int minCards = 60;
	public static final int maxCards = 250;
	
	public ConstructedDeckList(HashMap<String, Integer> deckList) {
		super(deckList);
	}
	public ConstructedDeckList(HashMap<String, Integer> deckList, String name) {
		super(deckList, name);
	}
	public ConstructedDeckList(String name) {
		super(name);
	}
	public ConstructedDeckList() {
		super();
	}
	
	@Override
	public void add(String string, int qty) {
		int k = -1;
		try {
			k = deckList.get(string).intValue();
		}
		catch(Exception e) {
			k = 0;
		}
		if(k > 0) {
			
			this.purge(string);
			if(this.checkLegality(string, k+qty)) {
				deckList.put(string, k+qty);
			}
			else {
				deckList.put(string, k);
			}
		}
		if(k == 0) {
			if(this.checkLegality(string, qty)) {
				deckList.put(string, qty);
			}
		}
		updateTable();
		
		Puller p = new Puller(this, string);
		Thread t = new Thread(p);
		t.start();
		
	}
	
	@Override
	public void purge(String string) {
		deckList.remove(string);
		updateTable();
	}

	@Override
	public void delete(String string, int qty) {
		if(deckList.get(string) != null) {
			int amnt = deckList.get(string);
			this.purge(string);
			deckList.put(string, amnt-qty);
		}
		updateTable();
	}
	
	public boolean correctAmount(String string, int qty) {
		boolean correctAmount = false;
		if(qty <= 4) {
			correctAmount = true;
		}
		else {
			if("Plains".equals(string)) {
				correctAmount = true;
			}
			if("Island".equals(string)) {
				correctAmount = true;
			}
			if("Swamp".equals(string)) {
				correctAmount = true;
			}
			if("Mountain".equals(string)) {
				correctAmount = true;
			}
			if("Forest".equals(string)) {
				correctAmount = true;
			}
			if("Rat Colony".equals(string)) {
				correctAmount = true;
			}
			if("Persistent Petitioners".equals(string)) {
				correctAmount = true;
			}
			if("Shadowborn Apostle".equals(string)) {
				correctAmount = true;
			}
			if("Relentless Rats".equals(string)) {
				correctAmount = true;
			}
		}
		return correctAmount;
	}
}
