package core;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;

import java.awt.Dimension;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;



/**
 *
 * @author gianm
 */
public class MainFrame extends MyJFrame {
	private BrowseFrame bf;
	private DeckList currentDeckList;
	private JTable currentTable;
   
	private static final long serialVersionUID = 1L;
	
    public MainFrame(BrowseFrame bf, Decks decks) {
    	super(decks);
    	this.bf = bf;
    	/*try {
			cs = new CardSearcher();
			decks = new Decks();
			// l System.out.println(decks.getAllDecks().get("UR Wizard Aggro").getDeckList().keySet().getClass());
			//System.out.println(cs.allCards.get("Counterspell").getColors()[0]);
		} catch (IOException e) {
			e.printStackTrace();
		}*/
    	this.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e) {
            	for(DeckList s : decks.getAllDecks().values()) {
            		s.toText("MTGA");
            	}
                e.getWindow().dispose();
            }
        });
    	
    	initComponents();
    }

    @SuppressWarnings("deprecation")
	private void initComponents() {

    	quitEditorButton = new javax.swing.JButton();
    	quitEditorButton.setForeground(Color.RED);
    	quitEditorButton.setBounds(282, 328, 89, 23);
    	quitEditorButton.setBackground(new java.awt.Color(255, 51, 51));
        quitEditorButton.setText("X");
        quitEditorButton.setPreferredSize(new java.awt.Dimension(23, 23));
        
        
        imgLabel = new javax.swing.JLabel();
        tablePanel = new javax.swing.JScrollPane();
        tablePanel.setBounds(399, 11, 299, 352);
        card = new javax.swing.JPanel();
        card.setBounds(0, 11, 252, 352);
    	
    	popupMenu = new JPopupMenu();
    	addMenu = new JMenuItem("add...");
    	removeMenu = new JMenuItem("remove...");
    	popupMenu.add(addMenu);
    	popupMenu.add(removeMenu);

    	currentTable = new JTable();
        basePanel = new javax.swing.JPanel();
        startPanel = new javax.swing.JPanel();
        
        menuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu("Decks");
        //editMenu = new javax.swing.JMenu("Return");
        aboutMenu = new javax.swing.JMenu("About");
    	debmNew = new javax.swing.JMenuItem("New");
    	subMenu = new JMenu("Recent...");
    	firstItem = new JMenuItem("");
    	secondItem = new JMenuItem("");
    	openList = new JMenuItem("Open...");
    	
    	menuUpdate();
    	
    	setJMenuBar(menuBar);
        menuBar.add(fileMenu);
        //menuBar.add(editMenu);
        menuBar.add(aboutMenu);
        fileMenu.add(debmNew);
        fileMenu.add(subMenu);
        fileMenu.add(openList);
        subMenu.add(firstItem);
        subMenu.add(secondItem);
        
        Object[] langs = {"English", "Italian"};
        JMenuItem lang = new JMenuItem("change language");
        aboutMenu.add(lang);
        lang.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	String format = (String) JOptionPane.showInputDialog((Component)getContentPane(),
                        (Object)"Select language","FormatSelection Dialog", JOptionPane.PLAIN_MESSAGE,
                        null,
	                    (Object[])langs, langs[0]);
            	
            	setLanguage(format);
        	}
        });
        
        /*JMenuItem a = new JMenuItem("main page");
        editMenu.add(a);
        a.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quitEditorButtonActionPerformed(evt);
            }
        });*/
        
        try {
			uriA = new URI("http://magic.wizards.com");
			uriB = new URI("http://reddit.com/r/magicTCG");
			uriC = new URI("http://scryfall.com");
			uriD = new URI("http://deckstats.net");
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        linkA = new JButton(uriA.toString());
        linkA.setBounds(0, 243, 344, 23);
        
        linkA.setBorderPainted(false);
        linkA.setVisible(true);
        linkA.setBackground(Color.WHITE);
        linkB = new JButton(uriB.toString());
        linkB.setBounds(344, 243, 344, 23);
        
        linkB.setBorderPainted(false);
        linkB.setOpaque(true);
        linkB.setBackground(Color.WHITE);
        linkD = new JButton(uriD.toString());
        linkD.setBounds(0, 272, 344, 23);
        
        linkD.setBorderPainted(false);
        linkD.setOpaque(true);
        linkD.setBackground(Color.WHITE);
        linkC = new JButton(uriC.toString());
        linkC.setBounds(344, 272, 344, 23);
        
        linkC.setBorderPainted(false);
        linkC.setOpaque(true);
        linkC.setBackground(Color.WHITE);
        linkC.addActionListener(new OpenUrlAction(uriC));
        linkD.addActionListener(new OpenUrlAction(uriD));
        linkB.addActionListener(new OpenUrlAction(uriB));
        linkA.addActionListener(new OpenUrlAction(uriA));
        linkB.setVisible(true);
        linkC.setVisible(true);
        linkD.setVisible(true);
        
        browseButton = new javax.swing.JButton();
        browseButton.setLocation(10, 11);
        browseButton.setSize(new Dimension(99, 205));
        browseButton.setMaximumSize(new Dimension(40, 60));
        
        deckEditorPanel = new javax.swing.JPanel();
        //blank = new javax.swing.JPanel();
        
        /*deckEditorButton.setLabel("Deck Editor");
        deckEditorButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deckEditorButtonActionPerformed(evt);
            }
        });*/

        browseButton.setLabel("Browse Cards");
        browseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
					browseButtonActionPerformed(evt);
				} catch (IOException e) {
					e.printStackTrace();
				}
            }
        });

        browseButton.getAccessibleContext().setAccessibleName("browseButton");
        
        fileMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUpdate();
            }
        });
        
        debmNew.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                String format = (String) JOptionPane.showInputDialog((Component)getContentPane(),
                        (Object)"Select a deck","FormatSelection Dialog", JOptionPane.PLAIN_MESSAGE,
                        null,
	                    (Object[])possibilities, possibilities[0]);
                String name = (String) JOptionPane.showInputDialog((Component)getContentPane(),
                        (Object)"Choose the new deck's name","NameSelection Dialog", JOptionPane.PLAIN_MESSAGE,
                        null,
	                    null, null);
                if(format.equals("Commander")) {
                	decks.add(name, new CommanderDeckList(name));
                }
                if(format.equals("Standard")) {
                	decks.add(name, new StandardConstructedDeckList(name));
                }
                if(format.equals("Vintage")) {
                	decks.add(name, new VintageConstructedDeckList(name));
    			}
                if(format.equals("Modern")) {
                	decks.add(name, new ModernConstructedDeckList(name));
    			}
                if(format.equals("Legacy")) {
                	decks.add(name, new LegacyConstructedDeckList(name));
    			}
                if(format.equals("Brawl")) {
                	decks.add(name, new LegacyConstructedDeckList(name));
    			}
                System.out.println("Created " + format + " deck");
                menuUpdate();
                openDeckEditor(name);
            }
        });

        
        openList.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String name = (String) JOptionPane.showInputDialog((Component)getContentPane(),
                        (Object)"Select a deck","FormatSelection Dialog", JOptionPane.PLAIN_MESSAGE,
                        null,
	                    (Object[])possibilities2, possibilities[0]);
				openDeckEditor(name);
			}
        });
        
        firstItem.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent evt) {
        		openDeckEditor(((JMenuItem)evt.getSource()).getText());
        	}
        });
        
        secondItem.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent evt) {
        		openDeckEditor(((JMenuItem)evt.getSource()).getText());
        	}
        });
        
        
        //remove Listener
    	removeMenu.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		String name = (String) JOptionPane.showInputDialog((Component)getContentPane(),
                        (Object)"How many?","NameSelection Dialog", JOptionPane.PLAIN_MESSAGE,
                        null,
	                    null, null);
        		int n = 1;
        		if(org.apache.commons.lang3.StringUtils.isNumeric(name)) {
        			n = Integer.parseInt(name);
        		}
        		currentDeckList.delete((String) currentTable.getValueAt(currentTable.getSelectedRow(), 0), n);	
        		menuUpdate(currentDeckList.getName());
        	}
        });
    	
        
        //add Listener
    	addMenu.addActionListener(new ActionListener() {
    	  	@Override
        	public void actionPerformed(ActionEvent e) {
        		String name = (String) JOptionPane.showInputDialog((Component)getContentPane(),
                        (Object)"How many?","NameSelection Dialog", JOptionPane.PLAIN_MESSAGE,
                        null,
	                    null, null);
        		int n = 1;
        		if(org.apache.commons.lang3.StringUtils.isNumeric(name)) {
        			n = Integer.parseInt(name);
        		}
        		currentDeckList.add((String) currentTable.getValueAt(currentTable.getSelectedRow(), 0), n);
        		menuUpdate(currentDeckList.getName());
        	}
        });

      
        quitEditorButton.addActionListener(new java.awt.event.ActionListener() {
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
        		quitEditorButtonActionPerformed(evt);
            }
        });
        
        
        basePanel.setLayout(new CardLayout());
        basePanel.setVisible(true);
        basePanel.add(startPanel, STARTER);
        basePanel.add(deckEditorPanel, DECKEDITOR);
        
        startPanel.setLayout(null);
        startPanel.add(browseButton);
        startPanel.add(linkA);
        startPanel.add(linkB);
        startPanel.add(linkC);
        startPanel.add(linkD);
        
        card.setLayout(new BorderLayout());
        card.add(imgLabel, java.awt.BorderLayout.CENTER);
        
        deckEditorPanel.setLayout(null);
        deckEditorPanel.add(card);
        deckEditorPanel.add(quitEditorButton);
        deckEditorPanel.add(tablePanel);

        getContentPane().add(basePanel);
        
        bl = (CardLayout) basePanel.getLayout();

        setPreferredSize(new Dimension(704, 412));
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        
        this.setResizable(false);
        pack();
    }
    
    private void browseButtonActionPerformed(java.awt.event.ActionEvent evt) throws IOException {
        bf.setVisible(true);
    }

    private void openDeckEditor(String name) {
    	menuUpdate(name);
    	currentDeckList = decks.getAllDecks().get(name);
    	
    	currentTable = currentDeckList.getTable();
    	DefaultTableModel md = currentDeckList.getModel();

        System.out.println(currentTable.getMouseListeners().length);
    	addToTable(currentTable);
    	
        bl.show(basePanel, DECKEDITOR);
        card.setVisible(true);
        
        System.out.println(currentTable.getMouseListeners().length);

    	if(currentTable.getMouseListeners().length <= 3) {
        currentTable.addMouseListener(new MouseAdapter() {
        	public void mousePressed(MouseEvent mouseEvent) {
        		JTable tab =(JTable) mouseEvent.getSource();
                //Point point = mouseEvent.getPoint();
                //int row = table.rowAtPoint(point);
                if (mouseEvent.getClickCount() == 1 && tab.getSelectedRow() != -1) {
                	String name = (String) md.getValueAt(currentTable.convertRowIndexToModel(currentTable.getSelectedRow()), 0);
                        	
                    BufferedImage image = null;
                    try {	
                        image = ImageIO.read(new File("resources/jpg/" + name + ".jpg"));
                        System.out.println("reading " + name);
                    } catch (IOException e) {
                    }
                    //ImageIcon icon = new ImageIcon (getClass().getResource("/resources/jpg/" + name + ".jpg").getImage());
                    Image newimage = image.getScaledInstance(252, 352, java.awt.Image.SCALE_SMOOTH);
                    ImageIcon icon = new ImageIcon(newimage);
                             
                    //overlayImgLabel.setMaximumSize(new Dimension(90, 160));
                    imgLabel.setIcon(icon);
                    imgLabel.setVisible(true);
                }
            }
        });
    	}
        System.out.println(currentTable.getMouseListeners().length);
        
    }

    private void quitEditorButtonActionPerformed(java.awt.event.ActionEvent evt) {
        bl.show(basePanel, STARTER);
        imgLabel.setIcon(null);
    }
    
    public void addToTable(JTable currentTable) {
    	if(currentTable.getRowCount() > 0) {
    		currentTable.addColumnSelectionInterval(0, 0);
        	currentTable.addRowSelectionInterval(0, 0);
    	}
    	tablePanel.setViewportView(currentTable);
    	
    	
    	//right click opens popup and selects row
    	if(currentTable.getMouseListeners().length <= 2) {
        currentTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                int r = currentTable.rowAtPoint(e.getPoint());
                if (r >= 0 && r < currentTable.getRowCount()) {
                    currentTable.setRowSelectionInterval(r, r);
                } else {
                    currentTable.clearSelection();
                }

                int rowindex = currentTable.getSelectedRow();
                if (rowindex < 0)
                    return;
                if (e.isPopupTrigger() && e.getComponent() instanceof JTable ) {
                    //JPopupMenu popup = createYourPopUp();
                    popupMenu.show(e.getComponent(), e.getX(), e.getY());
                }
            }
        });
    	}
    }
    
    public void menuUpdate(String name) {
    	if(!(firstItem.getText().equals(name))) {
    		decks.update(name);
    	}
    	firstItem.setText(decks.getTwoLatest().elementAt(1));
    	secondItem.setText(decks.getTwoLatest().elementAt(0));
    	possibilities2 = decks.getAllDecks().keySet().stream().toArray(Object[]::new);
    }
    public void menuUpdate() {
    	firstItem.setText(decks.getTwoLatest().elementAt(1));
    	secondItem.setText(decks.getTwoLatest().elementAt(0));
    	possibilities2 = decks.getAllDecks().keySet().stream().toArray(Object[]::new);
    }
    
    public void setLanguage(String format) {
    	//boolean language = "English".equals(format);
    	char num = 'a';
    	if("English".equals(format))
    		num = '1';
    	if("Italian".equals(format))
    		num = '0';
    	
    	FileWriter file = null;
    	
    	if(num != 'a') {
    		try {
    			file = new FileWriter("resources/lang/language.txt", false);
		
    			BufferedWriter a = new BufferedWriter(file);
			
    			a.write(num);
    			a.close();
    		}
    		catch (FileNotFoundException e1) {
    			e1.printStackTrace();
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
    	}
    }
    
 

    // Variables declaration
    private javax.swing.JButton browseButton;
    //private javax.swing.JButton deckEditorButton;
    private javax.swing.JButton quitEditorButton;
    
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenu fileMenu;
    //private javax.swing.JMenu editMenu;
    private javax.swing.JMenu aboutMenu;
    private JMenuItem debmNew;
    private JMenu subMenu;
    private JMenuItem firstItem;
    private JMenuItem secondItem;
    private JMenuItem openList;
    
    private JPopupMenu popupMenu;
    private JMenuItem addMenu;
    private JMenuItem removeMenu;
    
    private javax.swing.JPanel basePanel;
    private javax.swing.JPanel startPanel;
    private javax.swing.JPanel deckEditorPanel;
    private javax.swing.JScrollPane tablePanel;
    
    //private javax.swing.JPanel overlay;
    private javax.swing.JLabel imgLabel;
    private javax.swing.JPanel card;
    //private javax.swing.JPanel blank;
    
    private static Object[] possibilities = {"Commander", "Standard", "Vintage", "Modern", "Legacy", "Brawl"};
    private Object[] possibilities2;
    
    private CardLayout bl;
    //private CardLayout cl;
    
    //private javax.swing.JTable deckTable;
    //private DefaultTableModel model;
    //private TableRowSorter<TableModel> sorter;
    
    //private Loader u;
    //private Thread loaderThread;

    //private BufferedImage[] imgs;
    
    final static String STARTER = "Starter";
    final static String DECKEDITOR = "Deck Editor";
    
    final static String OVERLAYCARD = "Overlay Card";
    final static String BLANKCARD = "Blank Card";
    
    private JButton linkA;
    private JButton linkB;
    private JButton linkC;
    private JButton linkD;
    private URI uriA;
    private URI uriB;
    private URI uriC;
    private URI uriD;
}
