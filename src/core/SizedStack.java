package core;

import java.util.Stack;

@SuppressWarnings("hiding")
public class SizedStack<String> extends Stack<String>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int maxSize;

	public SizedStack(int size) {
	    super();
	    this.maxSize = size;
	}

	@Override
	public String push(String object) {
	    //If the stack is too big, remove elements until it's the right size.
	    while (this.size() >= maxSize) {
	        this.remove(0);
	    }
	    return super.push(object);
	}
	
}
