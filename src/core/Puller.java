package core;

public class Puller implements Runnable{
	private String name;
	private AbstractDeckList dl;
	private boolean go;
	
	public Puller(AbstractDeckList dl, String name) {
		this.name = name;
		this.dl = dl;
		go = true;
	}
	
	@Override
	public void run() {
		while(go) {
			go = dl.pullAndSaveImg(name);
		}
	}
}
