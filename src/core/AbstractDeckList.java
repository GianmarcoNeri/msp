package core;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public abstract class AbstractDeckList implements DeckList {
	protected HashMap<String, Integer> deckList = new HashMap<String, Integer>();
	protected String name = new String();
	
    protected DefaultTableModel model = new DefaultTableModel() {
        /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		@Override
        public boolean isCellEditable(int row, int column) {
           return false;
        }
    };
    protected JTable table = new JTable();
    private TableRowSorter<TableModel> sorter;
    
	public AbstractDeckList(HashMap<String, Integer> deckList) {
		this.deckList = deckList;
		tableSetUp();
	}
	public AbstractDeckList(HashMap<String, Integer> deckList, String name) {
		this.deckList = deckList;
		this.name = name;
		tableSetUp();
	}
	public AbstractDeckList(String name) {
		this.name = name;
		tableSetUp();
	}
	public AbstractDeckList() {
		Random rand = new Random();
		Integer n = rand.nextInt(99999999);
		this.name = n.toString();
		tableSetUp();
	}
	
	public void tableSetUp() {
		table.setModel(model);
		model.addColumn("name");
        model.addColumn("amount");
        sorter = new TableRowSorter<>(table.getModel());
        table.setRowSorter(sorter);
        List<RowSorter.SortKey> sortKeys = new ArrayList<>();
        int columnIndexToSort = 0;
        sortKeys.add(new RowSorter.SortKey(columnIndexToSort, SortOrder.ASCENDING));
        sorter.setSortKeys(sortKeys);
        sorter.sort();
        updateTable();
	}
	
	public void updateTable() {
		model.getDataVector().removeAllElements();
		model.fireTableDataChanged();
		for(String s : deckList.keySet()) {
    		model.addRow(new Object[] {s, deckList.get(s)});
    		//System.out.println(s + " " + deckList.get(s));
    	}
		model.fireTableDataChanged();
	}
	
	public HashMap<String, Integer> getDeckList(){
		return deckList;
	}
	public String getName() {
		return name;
	}
	public JTable getTable() {
    	return table;
    }
	public DefaultTableModel getModel() {
		return model;
	}
	
	public void toText(String mode) {
		if("MTGA".equals(mode)) {
			BufferedWriter writer = null;
			try {
	            //create a temporary file
	            File file = new File("resources/generated/" + name + ".txt");

	            // This will output the full path where the file will be written to...
	            //System.out.println(file.getCanonicalPath());

	            writer = new BufferedWriter(new FileWriter(file, false));
	            for(String n : deckList.keySet()) {		
					int i = deckList.get(n).intValue();
					writer.write(i + " " + n + "\n");
	            }
	        } 
			catch (Exception e) {
				e.printStackTrace();
	        } 
			finally {
				try {
	                writer.close();
	            } 
				catch (Exception e) {
	            }
	        }
		}
	}
	
	public void fromText(String name) {
		BufferedReader fileRead;
		File dir = new File("resources/" + name);
		boolean skipped = false;
		this.name = name.substring(0, name.length()-4); 
		try {
			fileRead = new BufferedReader(new FileReader(dir));
			String line = fileRead.readLine();
			while (line != null) {
				char[] a = line.toCharArray();
				double amnt = 0;
				String card = "";
				ArrayList<Integer> store = new ArrayList<Integer>();
				for(char c : a) {
					if(!skipped) {
						if (c == ' ') {
							skipped = true;
							continue;
						}
						int k = (int) c-48;
						
						store.add(new Integer(k));
					}
					if(skipped) {
						if (c == '(') {
							break;
						}
						card = card + c;
						
					}
				}
				
				if(card.length() > 0) {
					card = card.substring(0, card.length()-1);
				}
				
				
				for(Integer j = 1; j<=store.size(); j++) {
					Integer size = new Integer(store.size());
					amnt += store.get(j-1).doubleValue() * Math.pow(10.0, size.doubleValue() - j.doubleValue());	
				}
				
				//System.out.println("putting card " + card + " in amount " + amnt);
				if(! "".equals(card)) {
					Puller p = new Puller(this, card);
					Thread t = new Thread(p);
					t.start();
				}
				deckList.put(card, (int) amnt);
				amnt = 0;
				card = "";
				skipped = false;
				store = new ArrayList<Integer>();
				line = fileRead.readLine();
				
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		updateTable();
	}
	
	public boolean pullAndSaveImg(String name) {
		File outputfile = new File("resources/jpg/" + name + ".jpg");
		if(! outputfile.exists() ) {
			Card c = CardSearcher.allCards.get(name);	
			String req = "https://api.scryfall.com/cards/" + c.getUuid() + "?format=image&version=large";
			URL imguri;
			BufferedImage image = null;
			System.out.println(req);
 	
			try {

				imguri = new URL(req);
	
				image = ImageIO.read(imguri);
    		
    	
				// retrieve image
    		
				ImageIO.write(image, "jpg", outputfile);
    	
	 	 	
			}	
			catch (MalformedURLException e) {
				e.printStackTrace();
			}
			catch (javax.imageio.IIOException e) {
				e.printStackTrace();
			}
			catch (Exception e) {
    			e.printStackTrace();
    		}
			
		}
		return false;
	}
}
