package core;

import java.util.HashMap;

public class VintageConstructedDeckList extends ConstructedDeckList {
	
	public VintageConstructedDeckList(HashMap<String, Integer> deckList) {
		super(deckList);
	}
	public VintageConstructedDeckList(HashMap<String, Integer> deckList, String name) {
		super(deckList, name);
	}
	public VintageConstructedDeckList(String name) {
		super(name);
	}
	public VintageConstructedDeckList() {
		super();
	}

	public boolean checkLegality(String string, int qty) {
		boolean legal = false;
		String leg = CardSearcher.allCards.get(string).getLegalities().getVintage();
		if("Legal".equals(leg)) {
			legal = true && correctAmount(string, qty);
		}
		if("Restricted".equals(leg)) {
			legal = true && qty <= 1;
		}
		if("Banned".equals(leg)) {
			legal = false;
		}
		return legal;
	}
	
}
