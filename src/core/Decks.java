package core;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.commons.io.FilenameUtils;	

public class Decks {
	private HashMap<String, DeckList> allDecks = new HashMap<String, DeckList>();
	public SizedStack<String> twoLatest = new SizedStack<String>(2);
	
	public SizedStack<String> getTwoLatest() {
		return twoLatest;
	}

	public void setTwoLatest(SizedStack<String> twoLatest) {
		this.twoLatest = twoLatest;
	}

	public Decks() {
		this.readAllDecks();
	}

	public HashMap<String, DeckList> getAllDecks() {
		return allDecks;
	}

	public void setAllDecks(HashMap<String, DeckList> allDecks) {
		this.allDecks = allDecks;
	}
	
	
	public void add(String name, DeckList dl) {
		allDecks.put(name, dl);
		update(name);
	}
	
	public void update(String name) {
		twoLatest.push(name);
	}
	
	private void readAllDecks() {
		
		ArrayList<String> out = new ArrayList<String>();
		File directory= new File("resources");
		
		for (File file : directory.listFiles()){
			if (FilenameUtils.getExtension(file.getName()).equals("txt")){
				out.add(file.getName());
			}
		}
		
		for(String s : out) {
			
			
			DeckList mockDeck = new StandardConstructedDeckList();
			mockDeck.fromText(s);
			s = s.substring(0, s.length()-4);
			twoLatest.push(s);
			
			System.out.println("Adding deck: " + "\"" + s + "\"");
			
			allDecks.put(s, mockDeck);
			allDecks.get(s).toText("MTGA");
		}
		

		/*DeckList[] list = new DeckList[2];
		int i = 0;
    	for(DeckList d : this.getAllDecks().values()) {
    		list[i] = d;
    		for(DeckList e : this.getAllDecks().values()) {
    			if( e.getTime().after(list[i].getTime()) && !e.equals(list[i-1])) {
    				list[i] = e;
    			}
    		}
    		i++;
    	}*/
    	
	}
}
