package core;

import java.util.HashMap;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public interface DeckList {
	public HashMap<String, Integer> getDeckList();
	
	public String getName();
	
	public void add(String string, int qty);
	
	public void purge(String string);
	
	public void delete(String string, int qty);
	
	public boolean checkLegality(String string, int qty);
	
	public void toText(String mode);
	
	public void fromText(String name);
	
	public DefaultTableModel getModel();
	
	public JTable getTable();
}
