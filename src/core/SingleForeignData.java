package core;

public class SingleForeignData {
	private String flavorText;
	private String language;
	//private int multiverseID;
	private String name;
	private String text;
	private String type;	
	
	/*public int getMultiverseID() {
		return multiverseID;
	}
	public void setMultiverseID(int multiverseID) {
		this.multiverseID = multiverseID;
	}*/
	public String getFlavorText() {
		return flavorText;
	}
	public void setFlavorText(String flavorText) {
		this.flavorText = flavorText;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
