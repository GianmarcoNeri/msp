package core;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Card implements Comparable<Card> {
	
	private String[] colorIdentity;
	private String[] colors;
	private float convertedManaCost;
	private float faceConvertedManaCost;
	
	private String[] image_uris;

	private ArrayList<SingleForeignData> ForeignData;
	
	
	private boolean isReserved;
	private String layout;
	
	private Legalities legalities;
	
	private String manaCost;
	private String name;
	private String[] printings;
	
	private ArrayList<Ruling> rulings;
	
	private boolean starter;
	
	private String[] subtypes;
	private String[] supertypes;
	private String text;
	private String type;
	private String[] types;
	private String uuid; 
	
	private String power;
	private String toughness;
	
	private String[] names;
	
	private String side;
	
	private String loyalty;
	
	private String[] colorIndicator;
	
	/*card reader will build the card objects from AllCards.json*/
	public Card () {
		
	}
	
	@Override
	public int compareTo(Card card) {
		return 0;
	}

	public String[] getColorIdentity() {
		return colorIdentity;
	}

	public void setColorIdentity(String[] colorIdentity) {
		this.colorIdentity = colorIdentity;
	}

	public String[] getColors() {
		return colors;
	}

	public void setColors(String[] colors) {
		this.colors = colors;
	}

	public float getConvertedManaCost() {
		return convertedManaCost;
	}

	public void setConvertedManaCost(float convertedManaCost) {
		this.convertedManaCost = convertedManaCost;
	}
	
	public float getFaceConvertedManaCost() {
		return faceConvertedManaCost;
	}

	public void setFaceConvertedManaCost(float faceConvertedManaCost) {
		this.faceConvertedManaCost = faceConvertedManaCost;
	}

	public ArrayList<SingleForeignData> getForeignData() {
		return ForeignData;
	}

	public void setForeignData(ArrayList<SingleForeignData> foreignData) {
		ForeignData = foreignData;
	}

	@JsonProperty("isReserved")
	public boolean isReserved() {
		return isReserved;
	}

	public void setReserved(boolean isReserved) {
		this.isReserved = isReserved;
	}

	public String getLayout() {
		return layout;
	}

	public void setLayout(String layout) {
		this.layout = layout;
	}

	public Legalities getLegalities() {
		return legalities;
	}

	public void setLegalities(Legalities legalities) {
		this.legalities = legalities;
	}

	public String getManaCost() {
		return manaCost;
	}

	public void setManaCost(String manaCost) {
		this.manaCost = manaCost;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String[] getPrintings() {
		return printings;
	}

	public void setPrintings(String[] printings) {
		this.printings = printings;
	}

	public ArrayList<Ruling> getRulings() {
		return rulings;
	}

	public void setRulings(ArrayList<Ruling> rulings) {
		this.rulings = rulings;
	}
	
	

	public boolean isStarter() {
		return starter;
	}

	public void setStarter(boolean starter) {
		this.starter = starter;
	}

	public String[] getSubtypes() {
		return subtypes;
	}

	public void setSubtypes(String[] subtypes) {
		this.subtypes = subtypes;
	}

	public String[] getSupertypes() {
		return supertypes;
	}

	public void setSupertypes(String[] supertypes) {
		this.supertypes = supertypes;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String[] getTypes() {
		return types;
	}

	public void setTypes(String[] types) {
		this.types = types;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getPower() {
		return power;
	}

	public void setPower(String power) {
		this.power = power;
	}

	public String getToughness() {
		return toughness;
	}

	public void setToughness(String toughness) {
		this.toughness = toughness;
	}

	public String[] getNames() {
		return names;
	}

	public void setNames(String[] names) {
		this.names = names;
	}

	public String getSide() {
		return side;
	}

	public void setSide(String side) {
		this.side = side;
	}

	public String getLoyalty() {
		return loyalty;
	}

	public void setLoyalty(String loyalty) {
		this.loyalty = loyalty;
	}

	public String[] getColorIndicator() {
		return colorIndicator;
	}

	public void setColorIndicator(String[] colorIndicator) {
		this.colorIndicator = colorIndicator;
	}


	public String[] getImage_uris() {
		return image_uris;
	}

	public void setImage_uris(String[] image_uris) {
		this.image_uris = image_uris;
	}
	
	
}
