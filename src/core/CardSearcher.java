package core;

import java.io.IOException;
import java.util.HashMap;
import org.apache.commons.lang3.StringUtils;


public class CardSearcher {
	public static HashMap<String, Card> allCards;
	
	public CardSearcher(boolean language) throws IOException {
		if(language) {
			allCards = new CardReader().readCards();
		}
		if(!language) {
			allCards = new CardReader().readCardsIT();
		}
	}
	
	/*
	public CardList searchByName (MyMap cardList, String ricerca) {
		cl = new BrowseCardList();
		
		for(String s: cardList.keySet()) {
			if (s.contains(ricerca)){
				c = cardList.get(s);
				kvpair =  new MyMapEntry(s, c);
				cl.put(kvpair);
			}
		}
		return cl;
	}
	*/
	
	private HashMap<String, Card> resultList = new HashMap<String, Card>();
	//private HashMap<String, Card> tempList = new HashMap<String, Card>();
	
	@SuppressWarnings("unused")
	private Card currentCard;
	
	public HashMap<String, Card> search (HashMap<String, Card> cardList, String[] tokens, boolean[] modes){
		this.flushRL();
		for(String s: cardList.keySet()) {
			if(modes[1] == true || modes[2] == true) {
				currentCard = cardList.get(s);
				if ( modes[0] == true ) {
					if(nameMatch(s, tokens[0])) {
						if(isCorrectColor(currentCard, modes)) {
							resultList.put(s, currentCard);
						}
					}
				}
				if ( modes[1] == true ) {
					if(typeMatch(currentCard, tokens[0])) {
						if(isCorrectColor(currentCard, modes)) {
							resultList.put(s, currentCard);
						}
					}
				}
				if ( modes[2] == true ) {
					if(textMatch(currentCard, tokens[0])) {
						if(isCorrectColor(currentCard, modes)) {
							resultList.put(s, currentCard);
						}
					}
				}
			}
			
			else {
				if ( modes[0] == true ) {
					if(nameMatch(s, tokens[0])) {
						if(isCorrectColor(cardList.get(s), modes)) {
							resultList.put(s, cardList.get(s));
						}
					}
				}
			}
		}
		if(modes[9]) {
			filterByFormat(resultList, tokens[1]);
		}
		return resultList;
	}
	
	
	public HashMap<String, Card> search (String[] tokens, boolean[] modes){
		this.flushRL();
		for(String s: allCards.keySet()) {
			if(modes[1] == true || modes[2] == true) {
				currentCard = allCards.get(s);
				if ( modes[0] == true ) {
					if(nameMatch(s, tokens[0])) {
						if(isCorrectColor(currentCard, modes)) {
							resultList.put(s, currentCard);
						}
					}
				}
				if ( modes[1] == true ) {
					if(typeMatch(currentCard, tokens[0])) {
						if(isCorrectColor(currentCard, modes)) {
							resultList.put(s, currentCard);
						}
					}
				}
				if ( modes[2] == true ) {
					if(textMatch(currentCard, tokens[0])) {
						if(isCorrectColor(currentCard, modes)) {
							resultList.put(s, currentCard);
						}
					}
				}
			}
			else {
				if ( modes[0] == true ) {
					if(nameMatch(s, tokens[0])) {
						if(isCorrectColor(allCards.get(s), modes)) {
							resultList.put(s, allCards.get(s));
						}
					}
				}	
			}
		}
		if(modes[9]) {
			resultList = filterByFormat(resultList, tokens[1]);
		}
		return resultList;
	}
	
	public boolean nameMatch(String name, String token) {
		return StringUtils.containsIgnoreCase(name, token);
	}
	public boolean nameMatch(Card card, String token) {
		return StringUtils.containsIgnoreCase(card.getName(), token);
	}
	public boolean typeMatch(String name, String token) {
		return StringUtils.containsIgnoreCase(allCards.get(name).getType(), token);
	}
	public boolean typeMatch(Card card, String token) {
		return StringUtils.containsIgnoreCase(card.getType(), token);
	}
	public boolean textMatch(String name, String token) {
		return StringUtils.containsIgnoreCase(allCards.get(name).getText(), token);
	}
	public boolean textMatch(Card card, String token) {
		return StringUtils.containsIgnoreCase(card.getText(), token);
	}
	
	public HashMap<String, Card> searchByName (String token) {
		this.flushRL();
		for(String s: allCards.keySet()) {
			if(StringUtils.containsIgnoreCase(s, token)) {
				resultList.put(s, allCards.get(s)); 
			}
		}
		return resultList;
	}
	public HashMap<String, Card> searchByName (HashMap<String, Card> cardList, String token){
		this.flushRL();
		for(String s: cardList.keySet()) {
			if(StringUtils.containsIgnoreCase(s, token)) {
				resultList.putIfAbsent(s, cardList.get(s));
			}
		}
		return resultList;
	}
	
	public HashMap<String, Card> searchByType (HashMap<String, Card> cardList, String token){
		this.flushRL();
		for(String s: cardList.keySet()) {
			if (StringUtils.containsIgnoreCase(cardList.get(s).getType(), token)){
				resultList.put(s, cardList.get(s));
			}
		}
		return resultList;
	}
	public HashMap<String, Card> searchByType (String token){
		this.flushRL();
		for(String s: allCards.keySet()) {
			if (StringUtils.containsIgnoreCase(allCards.get(s).getType(), token)){
				resultList.put(s, allCards.get(s));
			}
		}
		return resultList;
	}
	
	public HashMap<String, Card> searchByText (HashMap<String, Card> cardList, String token){
		this.flushRL();
		for(String s: cardList.keySet()) {
			if (StringUtils.containsIgnoreCase(cardList.get(s).getText(), token)){
				resultList.put(s, cardList.get(s));
			}
		}
		return resultList;
	}
	public HashMap<String, Card> searchByText (String token){
		this.flushRL();
		for(String s: allCards.keySet()) {
			if (StringUtils.containsIgnoreCase(allCards.get(s).getText(), token)){
				resultList.put(s, allCards.get(s));
			}
		}
		return resultList;
	}
	
	// B G R U W json
	// W(3) U(4) B(5) R(6) G(7) standard
	public boolean isCorrectColor(Card card, boolean[] colors) {
		String[] cardColors = card.getColors();
		boolean[] checks = new boolean[5];
		for(int o = 0; o < 5; o++) {
			checks[o] = true;
		}
		
		boolean isCorrect = true;	//mock value for compiling
		
		//OR mode
		if(colors[8] == true) {
			isCorrect = true;
			for(String s : cardColors) {
				if("B".equals(s)) {
					if(colors[5] == true) {
						isCorrect = true;
						break;
					}
					continue;
				}
				if("G".equals(s)) {
					if(colors[7] == true) {
						isCorrect = true;
						break;
					}
					continue;
				}
				if("R".equals(s)) {
					if(colors[6] == true) {
						isCorrect = true;
						break;
					}
					continue;
				}
				if("U".equals(s)) {
					if(colors[4] == true) {
						isCorrect = true;
						break;
					}
					continue;
				} 
				if("W".equals(s)) {
					if(colors[3] == true) {
						isCorrect = true;
						break;
					}
					continue;
				}
			}
		}
		
		//AND mode
		if(colors[8] == false) {			
			isCorrect = true;			
			/*for(int k = 3; k < 8; k++) {
				if (colors[k] == true) {
					if (k == 3) {							//white
						if("W".equals(cardColors[4])) {
							continue;
						}
						isCorrect = false;
						break;
					}
					if (k == 4) {							//blue
						if("U".equals(cardColors[3])) {
							continue;
						}
						isCorrect = false;
						break;
					}
					if (k == 5) {							//black
						if("B".equals(cardColors[0])) {
							continue;
						}
						isCorrect = false;
						break;
					}
					if (k == 6) {							//red
						if("R".equals(cardColors[2])) {
							continue;
						}
						isCorrect = false;
						break;
					}
					if (k == 7) {							//green
						if("G".equals(cardColors[1])) {
							continue;
						}
						isCorrect = false;
						break;
					}
				}
			}*/
			
			
			for(int o = 0; o < 5; o++) {
				checks[o] = !colors[o+3];
			}
			
			for(String s : cardColors) {
				if("B".equals(s)) {
					if(colors[5] == true) {
						checks[2] = true;
						continue;
					}
				}
				if("G".equals(s)) {
					if(colors[7] == true) {
						checks[4] = true;
						continue;
					}
				}
				if("R".equals(s)) {
					if(colors[6] == true) {		
						checks[3] = true;
						continue;
					}
				}
				if("U".equals(s)) {
					if(colors[4] == true) {
						checks[1] = true;
						continue;
					}
				}
				if("W".equals(s)) {
					if(colors[3] == true) {
						checks[0] = true;
						continue;
					}	
				}
			}
		}
		
		for(int o = 0; o<5; o++) {
			isCorrect = isCorrect && checks[o];
		}
		return isCorrect;
	}
		
	
	
	/*public boolean isCorrectColorAND(Card card, boolean[] colors) {
		
		String[] cardColors = card.getColors();
		boolean isCorrect = true;
		
		for(String s : cardColors) {
			if("B".equals(s)) {
				if(colors[5] != true) {
					isCorrect = false;
					break;
				}
				continue;
			}
			if("G".equals(s)) {
				if(colors[7] != true) {
					isCorrect = false;
					break;
				}
				continue;
			}
			if("R".equals(s)) {
				if(colors[6] != true) {
					isCorrect = false;
					break;
				}
				continue;
			}
			if("U".equals(s)) {
				if(colors[4] != true) {
					isCorrect = false;
					break;
				}
				continue;
			}
			if("W".equals(s)) {
				if(colors[3] != true) {
					isCorrect = false;
					break;
				}
				continue;
			}
		}
		return isCorrect;
	}	*/
	
	public HashMap<String, Card> filterByFormat(HashMap<String, Card> cardList, String token){
		token = token.toLowerCase();
		
		Legalities l;
		String b = null;
		HashMap<String, Card> returnList = new HashMap<String, Card>();
		for(Card c : cardList.values()) {
			
			l = c.getLegalities();
			try {
					b = l.getLegality(token);
				if(! ("Banned".equals(b) || b == null)) {
					returnList.put(c.getName(), c);
				}
				/*else {
					System.out.println(c.getName());
				}*/
				
			} catch (SecurityException | IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return returnList;
	}
	
	public HashMap<String, Card> filterByColor (HashMap<String, Card> cardList, boolean[] colors){
		this.flushRL();
		String[] colorsCheck = {"B", "G", "R", "U", "W"};
		for (int i = 3; i<8; i++) {
			if (!colors[i]) {
				colorsCheck[i-3] = "X";
			}
		}
		
		String colorsCheckString = String.join(",", colorsCheck);
		
		for(Card c: cardList.values()) {
			if(colors[8]) {
				String[] currentCardColors = c.getColors();
				String currentCardColorsString = String.join(",", currentCardColors);
				if(StringUtils.containsAny(currentCardColorsString, colorsCheckString)) {
					cardList.remove(c.getName());
				}
			}
			if(!colors[8]) {
				String[] currentCardColors = c.getColors();
				String currentCardColorsString = String.join(",", currentCardColors);
				if(!StringUtils.containsOnly(currentCardColorsString, colorsCheckString)) {
					cardList.remove(c.getName());
				}
				
			}
		}
		return cardList;
	}
	
	private void flushRL() {
		resultList.clear();
	}
}
