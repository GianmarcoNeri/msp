package core;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.*;


public class CardReader {
		
	//public static MyMap cards;
	private HashMap<String, Card> cards;

	public CardReader() {
		/*byte[] jsonData = Files.readAllBytes(Paths.get("resources/AllCards.json"));
		
		ObjectMapper mapper = new ObjectMapper();
		
		MyMap cards = mapper.readValue(jsonData, MyMap.class);
		
		Card test = (Card) cards.get("Chemister's Insight");
		
		System.out.println(test.getForeignData().get(3).getFlavorText());*/
		
		//Card card = mapper.readValue(new File("resources/AllCards.json"), Card.class);
		//System.out.println(card.name);
		/*FileReader fr = new FileReader("resources/AllCards.json");
		Object obj = new JSONParser().parse(fr);
		JSONObject jo = (JSONObject) obj;
		String name = (String) jo.get("name");
		System.out.println(name);*/
		//JSONObject obj = new JSONObject("resources/AllCards.json");
		//System.out.println((int)obj.toString().charAt(0));
		//String name = obj.getJSONObject("Ach! Hans, Run!").getString("name");
		//System.out.println(name);
	}
	
	public HashMap<String, Card> readCards() {
		byte[] jsonData;
		try {
			jsonData = Files.readAllBytes(Paths.get("resources/AllCards.json"));
			ObjectMapper mapper = new ObjectMapper();
			
			cards = mapper.readValue(jsonData, new TypeReference<HashMap<String, Card>>() {});
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return cards;
		
		//Card test = (Card) cards.get("Chemister's Insight");
		
		//System.out.println(test.getForeignData().get(3).getFlavorText());
		
	}
	public HashMap<String, Card> readCardsIT() {
		byte[] jsonData;
		try {
			jsonData = Files.readAllBytes(Paths.get("resources/AllCards.json"));
			ObjectMapper mapper = new ObjectMapper();
			
			cards = mapper.readValue(jsonData, new TypeReference<HashMap<String, Card>>() {});
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		HashMap<String, Card> updatedCards = new HashMap<String, Card>();
		
		for(String s: cards.keySet()) {
			Card cc = cards.get(s);
			SingleForeignData cfi = null;
			//System.out.println(cc.getName());
			if(cc.getForeignData().size() >= 4) {
				cfi = cc.getForeignData().get(3);
				
				if(!(null == cfi.getName())){
					cc.setName(cfi.getName());
				}
				if(!(null == cfi.getText())){
					cc.setText(cfi.getText());
				}
				if(!(null == cfi.getType())){
					cc.setType(cfi.getType());
				}
				updatedCards.put(cfi.getName(), cc);
			}
			//System.out.println(cc.getName());
		}
		
		return updatedCards;
		
		//Card test = (Card) cards.get("Chemister's Insight");
		
		//System.out.println(test.getForeignData().get(3).getFlavorText());
		
	}
	
}
