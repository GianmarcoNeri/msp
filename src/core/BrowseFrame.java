package core;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.jdesktop.swingx.JXCollapsiblePane;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Color;
import java.awt.Component;
import javax.swing.SwingConstants;

/**
 *
 * @author gianm
 */
public class BrowseFrame extends MyJFrame {	
	private CardSearcher cs;
	private static final long serialVersionUID = 1L;
	
    public BrowseFrame(Decks decks, CardSearcher cs) {
    	super(decks);
    	this.cs = cs;
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    
    private void initComponents() {
    	//Variables Initialization
    	tokens = new String[3];
    	searchModes = new boolean[10];
    	for(int i = 0; i<=8; i++) {
    		if(i == 0)						//0=name
    			searchModes[i] = true;
    		if(i < 3 && i > 0)				//1=type 2=text
    			searchModes[i] = false;
    		if(i >= 3 && i <=7)				//3-4-5-6-7 colors
    			searchModes[i] = true;
    		if(i==8)						//8 color mode
    			searchModes[i] = true;
    		if(i == 9)						//9 format diverso da all
    			searchModes[i] = false;
    	}
    	
    	resultList = new HashMap<String, Card>();
    	latestResultList = new HashMap<String, Card>();
    	
    	File f = new File("resources/blank.jpg");
    	try {
			blankImage = ImageIO.read(f);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        Image newimage = blankImage.getScaledInstance(252, 352,  java.awt.Image.SCALE_SMOOTH);
        blankIcon = new ImageIcon(newimage);
    	
        searchTextField = new JTextField();
        jWhiteCheck = new JCheckBox();
        jBlueCheck = new JCheckBox();
        jBlackCheck = new JCheckBox();
        jRedCheck = new JCheckBox();
        jGreenCheck = new JCheckBox();
        jNameCheck = new JCheckBox();
        jTypeCheck = new JCheckBox();
        jTextCheck = new JCheckBox();
        formatCombo = new JComboBox<>();
        formatCombo.setModel(new DefaultComboBoxModel<>(new String[] { "All", "Standard", "Modern", "Commander", "Pauper", "Legacy", "Vintage", "Brawl"}));
        formatCombo.setSelectedIndex(0);
    	comboColorMode = new JComboBox<>();
        comboColorMode.setModel(new DefaultComboBoxModel<>(new String[] {"OR", "AND"}));
        
        searchButton = new JButton();
        toggleSearchButton = new JButton();
        jBtnCloseOverlay = new JButton();
        jBtnCloseOverlay.setBounds(735, 11, 39, 23);
        jBtnCloseOverlay.setVerticalAlignment(SwingConstants.TOP);
        jBtnCloseOverlay.setHorizontalAlignment(SwingConstants.LEADING);
        
        basePanel = new JPanel();
        basePanel.setBackground(new Color(0, 0, 51));
        jXCollapsiblePane1 = new org.jdesktop.swingx.JXCollapsiblePane();
        collapsiblePanelContentPane = new JPanel();
        tablePanel = new JPanel();
        tablePanel.setBackground(new Color(0, 0, 40));
        tablePanel.setForeground(new Color(0, 0, 51));
        jScrollPane1 = new JScrollPane();
    	overlayPanel =  new javax.swing.JPanel();
    	overlayPanel.setBackground(Color.DARK_GRAY);
        
        setLabel = new JTextArea();
        setLabel.setEditable(false);
        setLabel.setVisible(true);
        setLabel.setBounds(601, 66, 173, 171);
        setLabel.setBackground(Color.WHITE);
        overlayImgLabel = new JLabel();
        overlayImgLabel.setBounds(240, 45, 252, 352);
        
        popupMenu = new JPopupMenu(); 
        popupMenu.add(viewFocusMenu = new JMenuItem("View Focus"));
        popupMenu.add(new JPopupMenu.Separator());
        popupMenu.add(subMenu = new JMenu("Add to Deck...>"));
    	subMenu.add(firstDeckMenu = new JMenuItem());
        subMenu.add(secondDeckMenu = new JMenuItem());  
        menuUpdate();
        
        model = new DefaultTableModel() {
			private static final long serialVersionUID = 1L;
			
			@Override
            public boolean isCellEditable(int row, int column) {
               return false;
            }
        };   
        resultTable = new JTable(model);
        model.addColumn("name");
        model.addColumn("cmc");
        model.addColumn("types");
        sorter = new TableRowSorter<>(resultTable.getModel());
        resultTable.setRowSorter(sorter);
        List<RowSorter.SortKey> sortKeys = new ArrayList<>();
        int columnIndexToSort = 0;
        sortKeys.add(new RowSorter.SortKey(columnIndexToSort, SortOrder.ASCENDING));
        sorter.setSortKeys(sortKeys);
        sorter.sort();
        
        searchTextField.setText("search terms");
        Color faded = new Color(211, 211, 211, 225);
        Color black = new Color(0, 0, 0, 255);
        searchTextField.setForeground(faded);
        searchTextField.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent arg0) {
				if(searchTextField.getText().equals("search terms")) {
					searchTextField.setText(""); 
		        	searchTextField.setForeground(black);	
				}
			}
			@Override
			public void focusLost(FocusEvent arg0) {
			}
        });
        
        jNameCheck.setText("Name");
        jNameCheck.setSelected(true);
        jTypeCheck.setText("Type");
        jTextCheck.setText("Text");
        jWhiteCheck.setText("White");
        jWhiteCheck.setOpaque(true);
        jWhiteCheck.setBackground(new Color(249, 250, 244));
        jWhiteCheck.setSelected(true);
        jWhiteCheck.setMaximumSize(new java.awt.Dimension(55, 23));
        jWhiteCheck.setMinimumSize(new java.awt.Dimension(55, 23));
        jWhiteCheck.setPreferredSize(new java.awt.Dimension(55, 23));
        jBlueCheck.setText("Blue");
        jBlueCheck.setOpaque(true);
        jBlueCheck.setBackground(new Color(14, 104, 171));
        jBlueCheck.setSelected(true);
        jBlueCheck.setMaximumSize(new java.awt.Dimension(55, 23));
        jBlueCheck.setMinimumSize(new java.awt.Dimension(55, 23));
        jBlueCheck.setPreferredSize(new java.awt.Dimension(55, 23));
        jBlackCheck.setText("Black");
        jBlackCheck.setOpaque(true);
        jBlackCheck.setBackground(new Color(21, 11, 0));
        jBlackCheck.setForeground(new Color(255, 255, 255));
        jBlackCheck.setSelected(true);
        jBlackCheck.setMaximumSize(new java.awt.Dimension(55, 23));
        jBlackCheck.setMinimumSize(new java.awt.Dimension(55, 23));
        jBlackCheck.setPreferredSize(new java.awt.Dimension(55, 23));        
        jRedCheck.setText("Red");
        jRedCheck.setOpaque(true);
        jRedCheck.setBackground(new Color(211, 32, 42));
        jRedCheck.setSelected(true);
        jRedCheck.setMaximumSize(new java.awt.Dimension(55, 23));
        jRedCheck.setMinimumSize(new java.awt.Dimension(55, 23));
        jRedCheck.setPreferredSize(new java.awt.Dimension(55, 23));
        jGreenCheck.setText("Green");
        jGreenCheck.setOpaque(true);
        jGreenCheck.setBackground(new Color(0, 115, 42));
        jGreenCheck.setSelected(true);
        jGreenCheck.setMaximumSize(new java.awt.Dimension(55, 23));
        jGreenCheck.setMinimumSize(new java.awt.Dimension(55, 23));
        jGreenCheck.setPreferredSize(new java.awt.Dimension(55, 23));
                     
        searchButton.setText("Search");
        toggleSearchButton.setText("Toggle Search");
        jBtnCloseOverlay.setText("\r\nX");
        
        jNameCheck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	searchModes[0] = jNameCheck.isSelected();
            }
        });
        jTypeCheck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	searchModes[1] = jTypeCheck.isSelected();
            }
        });
        jTextCheck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	searchModes[2] = jTextCheck.isSelected();
            }
        });   
        jWhiteCheck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	searchModes[3] = jWhiteCheck.isSelected();
            }
        });
        jBlueCheck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	searchModes[4] = jBlueCheck.isSelected();
            }
        });
        jBlackCheck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	searchModes[5] = jBlackCheck.isSelected();
            }
        });
        jRedCheck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	searchModes[6] = jRedCheck.isSelected();
            }
        });
        jGreenCheck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	searchModes[7] = jGreenCheck.isSelected();
            }
        });
        searchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchButtonActionPerformed(evt);
            }
        });
        formatCombo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				tokens[1] = (String) formatCombo.getSelectedItem();
				if(! "All".equals(tokens[1]) ) {
					searchModes[9] = true;
				}
				else {
					searchModes[9] = false;
				}
			}
        });
        comboColorMode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	searchModes[8] = (comboColorMode.getSelectedIndex() == 0);
            	System.out.println(searchModes[8]);
            }
        });
        jBtnCloseOverlay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnCloseOverlayActionPerformed(evt);
            }
        });
        
        //menu listeners
        viewFocusMenu.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		showFocus(latestResultList.get(resultTable.getValueAt(resultTable.getSelectedRow(), 0)));
        	}
        });
        firstDeckMenu.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		String name = (String) JOptionPane.showInputDialog((Component)getContentPane(),
                        (Object)"How many?","NameSelection Dialog", JOptionPane.PLAIN_MESSAGE,
                        null,
	                    null, null);
        		int n = 1;
        		if(org.apache.commons.lang3.StringUtils.isNumeric(name)) {
        			n = Integer.parseInt(name);
        		}
        		decks.getAllDecks().get(firstDeckMenu.getText()).add((String) resultTable.getValueAt(resultTable.getSelectedRow(), 0), n);
        		menuUpdate(firstDeckMenu.getText());
        			
        	}
        });
        secondDeckMenu.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		String name = (String) JOptionPane.showInputDialog((Component)getContentPane(),
                        (Object)"How many?","NameSelection Dialog", JOptionPane.PLAIN_MESSAGE,
                        null,
	                    null, null);
        		int n = 1;
        		if(org.apache.commons.lang3.StringUtils.isNumeric(name)) {
        			n = Integer.parseInt(name);
        		}
        		decks.getAllDecks().get(secondDeckMenu.getText()).add((String) resultTable.getValueAt(resultTable.getSelectedRow(), 0), n);
        		menuUpdate(secondDeckMenu.getText());
        	}
        });
        
        //right click opens popup and selects row
        resultTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                int r = resultTable.rowAtPoint(e.getPoint());
                if (r >= 0 && r < resultTable.getRowCount()) {
                    resultTable.setRowSelectionInterval(r, r);
                } else {
                    resultTable.clearSelection();
                }

                int rowindex = resultTable.getSelectedRow();
                if (rowindex < 0)
                    return;
                if (e.isPopupTrigger() && e.getComponent() instanceof JTable ) {
                    //JPopupMenu popup = createYourPopUp();
                    popupMenu.show(e.getComponent(), e.getX(), e.getY());
                }
            }
            @Override
            public void mousePressed(MouseEvent e) {
            	menuUpdate();
            }
        });
        
        //double click to showFocus
        resultTable.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent mouseEvent) {
                JTable table =(JTable) mouseEvent.getSource();
                //Point point = mouseEvent.getPoint();
                //int row = table.rowAtPoint(point);
                if (mouseEvent.getClickCount() == 2 && table.getSelectedRow() != -1) {
                	String name = (String) model.getValueAt(resultTable.convertRowIndexToModel(table.getSelectedRow()), 0);
                	//HashMap<String, Card> targetCard = cs.searchByName(latestResultList, name);
                	showFocus(latestResultList.get(name));
                	
                }
            }
        });
        
        javax.swing.GroupLayout collapsiblePanelContentPaneLayout = new javax.swing.GroupLayout(collapsiblePanelContentPane);
        collapsiblePanelContentPaneLayout.setHorizontalGroup(
        	collapsiblePanelContentPaneLayout.createParallelGroup(Alignment.LEADING)
        		.addGroup(collapsiblePanelContentPaneLayout.createSequentialGroup()
        			.addGroup(collapsiblePanelContentPaneLayout.createParallelGroup(Alignment.LEADING, false)
        				.addGroup(Alignment.TRAILING, collapsiblePanelContentPaneLayout.createSequentialGroup()
        					.addGap(25)
        					.addComponent(searchTextField)
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addComponent(searchButton))
        				.addGroup(collapsiblePanelContentPaneLayout.createSequentialGroup()
        					.addGap(115)
        					.addComponent(formatCombo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
        			.addGap(44)
        			.addComponent(comboColorMode, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.UNRELATED)
        			.addGroup(collapsiblePanelContentPaneLayout.createParallelGroup(Alignment.LEADING, false)
        				.addGroup(collapsiblePanelContentPaneLayout.createSequentialGroup()
        					.addComponent(jWhiteCheck, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        					.addPreferredGap(ComponentPlacement.UNRELATED)
        					.addComponent(jBlueCheck, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        					.addPreferredGap(ComponentPlacement.UNRELATED)
        					.addComponent(jBlackCheck, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        				.addGroup(collapsiblePanelContentPaneLayout.createSequentialGroup()
        					.addComponent(jNameCheck, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
        					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        					.addComponent(jTypeCheck, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)))
        			.addGroup(collapsiblePanelContentPaneLayout.createParallelGroup(Alignment.LEADING)
        				.addGroup(collapsiblePanelContentPaneLayout.createSequentialGroup()
        					.addGap(2)
        					.addComponent(jRedCheck, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        					.addPreferredGap(ComponentPlacement.UNRELATED)
        					.addComponent(jGreenCheck, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        				.addGroup(collapsiblePanelContentPaneLayout.createSequentialGroup()
        					.addGap(29)
        					.addComponent(jTextCheck, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)))
        			.addGap(110))
        );
        collapsiblePanelContentPaneLayout.setVerticalGroup(
        	collapsiblePanelContentPaneLayout.createParallelGroup(Alignment.LEADING)
        		.addGroup(collapsiblePanelContentPaneLayout.createSequentialGroup()
        			.addContainerGap()
        			.addGroup(collapsiblePanelContentPaneLayout.createParallelGroup(Alignment.TRAILING)
        				.addGroup(collapsiblePanelContentPaneLayout.createSequentialGroup()
        					.addGroup(collapsiblePanelContentPaneLayout.createParallelGroup(Alignment.BASELINE)
        						.addComponent(jWhiteCheck, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        						.addComponent(jBlueCheck, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
        						.addComponent(jBlackCheck, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        						.addComponent(jRedCheck, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        						.addComponent(jGreenCheck, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        						.addComponent(comboColorMode, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        					.addGap(16)
        					.addGroup(collapsiblePanelContentPaneLayout.createParallelGroup(Alignment.BASELINE)
        						.addComponent(jNameCheck)
        						.addComponent(jTypeCheck)
        						.addComponent(jTextCheck)))
        				.addGroup(collapsiblePanelContentPaneLayout.createSequentialGroup()
        					.addGroup(collapsiblePanelContentPaneLayout.createParallelGroup(Alignment.BASELINE)
        						.addComponent(searchTextField, GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
        						.addComponent(searchButton))
        					.addGap(18)
        					.addComponent(formatCombo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
        			.addContainerGap())
        );
        
        collapsiblePanelContentPane.setLayout(collapsiblePanelContentPaneLayout);
        collapsiblePanelContentPane.setBackground(new Color(255, 165, 0));

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(800, 600));

        jXCollapsiblePane1.setLayout(new BorderLayout());
        jXCollapsiblePane1.add(collapsiblePanelContentPane, BorderLayout.CENTER);
        
        toggleSearchButton.addActionListener(jXCollapsiblePane1.getActionMap().get(JXCollapsiblePane.TOGGLE_ACTION));
        
        basePanel.setLayout(new CardLayout());
        basePanel.setVisible(true);
        
        getContentPane().add(jXCollapsiblePane1, java.awt.BorderLayout.PAGE_START);
        getContentPane().add(toggleSearchButton, java.awt.BorderLayout.PAGE_END);
        basePanel.add(overlayPanel, OVERLAYPANEL);
        overlayPanel.setLayout(null);
        overlayPanel.add(overlayImgLabel);
        
        scrollSetLabel = new JScrollPane();
        scrollSetLabel.setBounds(592, 45, 182, 223);
        overlayPanel.add(scrollSetLabel);
        scrollSetLabel.setViewportView(setLabel);
        overlayPanel.add(jBtnCloseOverlay);
        
        jScrollPane1.setViewportView(resultTable);
        tablePanel.add(jScrollPane1);
        basePanel.add(tablePanel, TABLEPANEL); 
        
        getContentPane().add(basePanel);
        
        cl = (CardLayout) basePanel.getLayout();
        cl.show(basePanel, TABLEPANEL);
        searchButton.requestFocusInWindow();
        
        this.setResizable(false);
        pack();
    }

    private void searchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_toggleSearchButtonActionPerformed
    	HashMap<String, Card> resultList = new HashMap<String, Card>();
    	tokens[0] = searchTextField.getText();
    	//tokens[1] = tokens[0];
    	tokens[2] = tokens[0];
    	
    	resultList = cs.search(tokens, searchModes);

    	resultTableflush();
    	resultTableadd(resultList);
    	
    	latestResultList = resultList;  	
    }
        
    private void resultTableadd(HashMap<String, Card> resultList) {
    	int i = 0;
    	for(String s : resultList.keySet()) {
    		Card c = resultList.get(s);
    		model.addRow(new Object[] {s, c.getConvertedManaCost(), c.getType()});
    		i++;
    	}
    	
    	System.out.println(i);
    }
    
    private void resultTableflush() {
    	model.getDataVector().removeAllElements();
    	model.fireTableDataChanged();
    }
    
    private void jBtnCloseOverlayActionPerformed(java.awt.event.ActionEvent evt) {                                           
    	cl.show(basePanel, TABLEPANEL);
    }
    
    /*private void comboColorModeActionPerformed(java.awt.event.ActionEvent evnt) {
    	searchModes[8] = (comboColorMode.getSelectedIndex() == 0);
    	System.out.println(searchModes[8]);
    }*/
    
    public void showFocus(Card c) {
    	
    	cl.show(basePanel, OVERLAYPANEL);
    	
    	String req = "https://api.scryfall.com/cards/" + c.getUuid() + "?format=image&version=large";
    	URL imguri;
     	BufferedImage image = null;
     	System.out.println(req);
	 	
		try {

			imguri = new URL(req);
		
    	 	image = ImageIO.read(imguri);
    	 	 	
		}	
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
		catch (javax.imageio.IIOException e) {
			e.printStackTrace();
			overlayImgLabel.setIcon(blankIcon);
			overlayImgLabel.setVisible(true);
	        String text = new String("Printings:\n");
	        setLabel.setText(text);
			return;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		//System.out.println(image);
        //System.out.println("Load image into frame...");
        
        Image newimage = image.getScaledInstance(252, 352,  java.awt.Image.SCALE_SMOOTH);
        ImageIcon icon = new ImageIcon(newimage);
         
        //overlayImgLabel.setMaximumSize(new Dimension(90, 160));
        overlayImgLabel.setIcon(icon);
        overlayImgLabel.setVisible(true);
         
        String text = new String("Printings:\n");
        int i = 0;
        for(String s : c.getPrintings()) {
        	text += s + "; ";
        	i++;
        	if(i==4) {
        		text += "\n";
        		i = 0;
        	}
        }
        setLabel.setText(text);
    }
    
    public void addToDeck(String name) {
    	//decks.getAllDecks().get(name).add(string, qty);
    }
    
    public void menuUpdate(String name) {
    	if(!(firstDeckMenu.getText().equals(name))) {
    		decks.update(name);
    	}
    	firstDeckMenu.setText(decks.getTwoLatest().elementAt(1));
    	secondDeckMenu.setText(decks.getTwoLatest().elementAt(0));
    }
    public void menuUpdate() {
    	firstDeckMenu.setText(decks.getTwoLatest().elementAt(1));
    	secondDeckMenu.setText(decks.getTwoLatest().elementAt(0));
    }

    // Variables declaration
    private JButton toggleSearchButton;
    private JButton searchButton;
    private JButton jBtnCloseOverlay;
    
    private JCheckBox jWhiteCheck;
    private JCheckBox jBlueCheck;
    private JCheckBox jBlackCheck;
    private JCheckBox jRedCheck;
    private JCheckBox jGreenCheck;
    private JCheckBox jNameCheck;
    private JCheckBox jTypeCheck;
    private JCheckBox jTextCheck;
    private JComboBox<String> formatCombo;
    private JComboBox<String> comboColorMode;
    
    private CardLayout cl;
    private JPanel basePanel;
    private org.jdesktop.swingx.JXCollapsiblePane jXCollapsiblePane1;
    private JPanel collapsiblePanelContentPane;
    private JPanel tablePanel;
    private JScrollPane jScrollPane1;
    private JPanel overlayPanel;
    final static String TABLEPANEL = "Card with JTable";
    final static String OVERLAYPANEL = "Overlay Card";
 
    private JTable resultTable;
    private DefaultTableModel model;
    private TableRowSorter<TableModel> sorter;
    
    private JTextField searchTextField;
    private JTextArea setLabel;
    private JLabel overlayImgLabel;
    private JScrollPane scrollSetLabel;
    
    @SuppressWarnings("unused")
	private HashMap<String, Card> resultList;
	private HashMap<String, Card> latestResultList;
    private boolean[] searchModes;
    private String[] tokens;
    
    BufferedImage blankImage;
    ImageIcon blankIcon;
    
    private JPopupMenu popupMenu;
    private JMenuItem viewFocusMenu;
    private JMenuItem firstDeckMenu;
    private JMenuItem secondDeckMenu;
    private JMenu subMenu; 
    // End of variables declaration
}
