package core;

import java.util.HashMap;

public class LegacyConstructedDeckList extends ConstructedDeckList{

	public LegacyConstructedDeckList(HashMap<String, Integer> deckList) {
		super(deckList);
	}
	public LegacyConstructedDeckList(HashMap<String, Integer> deckList, String name) {
		super(deckList, name);
	}
	public LegacyConstructedDeckList(String name) {
		super(name);
	}
	public LegacyConstructedDeckList() {
		super();
	}
	
	public boolean checkLegality(String string, int qty) {
		return ("Legal".equals(CardSearcher.allCards.get(string).getLegalities().getLegacy())) && (correctAmount(string, qty));
	}
	
	/*public void purge(String string) {
		deckList.remove(string);	
	}

	public void delete(String string, int qty) {
		if(deckList.get(string).intValue() > 0) {
			int amnt = deckList.get(string);
			this.purge(string);
			deckList.put(string, amnt-qty);
		}
		else {
			if(this.checkLegality(string, qty)) {
				deckList.put(string, qty);
			}
		}
	}*/
}
