package core;

import java.util.HashMap;

public class BrawlDeckList extends SingletonDeckList {
	public static final int minCards = 60;
	public static final int maxCards = 60;
	
	public BrawlDeckList(HashMap<String, Integer> deckList) {
		super(deckList);
	}
	public BrawlDeckList(HashMap<String, Integer> deckList, String name) {
		super(deckList, name);
	}
	public BrawlDeckList(String name) {
		super(name);
	}
	public BrawlDeckList() {
		super();
	}

	@Override
	public boolean checkLegality(String string, int qty) {
		boolean format = ("Legal".equals(CardSearcher.allCards.get(string).getLegalities().getBrawl()));
		boolean legal = format && (correctAmount(string, qty));
		return legal;		
	}
	
}
