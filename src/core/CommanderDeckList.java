package core;

import java.util.HashMap;

public class CommanderDeckList extends SingletonDeckList {
	public static final int minCards = 100;
	public static final int maxCards = 100;
	
	public CommanderDeckList(HashMap<String, Integer> deckList) {
		super(deckList);
	}
	public CommanderDeckList(HashMap<String, Integer> deckList, String name) {
		super(deckList, name);
	}
	public CommanderDeckList(String name) {
		super(name);
	}
	public CommanderDeckList() {
		super();
	}

	@Override
	public boolean checkLegality(String string, int qty) {
		boolean format = ("Legal".equals(CardSearcher.allCards.get(string).getLegalities().getCommander()));
		boolean legal = format && (correctAmount(string, qty));
		return legal;		
	}
	
}
