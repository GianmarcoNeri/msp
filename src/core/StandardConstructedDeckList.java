package core;

import java.util.HashMap;

public class StandardConstructedDeckList extends ConstructedDeckList {
	
	public StandardConstructedDeckList(HashMap<String, Integer> deckList) {
		super(deckList);
	}	
	public StandardConstructedDeckList(HashMap<String, Integer> deckList, String name) {
		super(deckList, name);
	}
	public StandardConstructedDeckList(String name) {
		super(name);
	}
	public StandardConstructedDeckList() {
		super();
	}
	
	public boolean checkLegality(String string, int qty) {
		boolean legal = ("Legal".equals(CardSearcher.allCards.get(string).getLegalities().getStandard())) && (correctAmount(string, qty));
		return legal;
	}
	
	/*@Override
	public void add(String string, int qty) {
		if(deckList.get(string).intValue() > 0) {
			int amnt = deckList.get(string);
			this.purge(string);
			if(this.checkLegality(string, amnt+qty)) {
				deckList.put(string, amnt+qty);
			}
		}
		else {
			if(this.checkLegality(string, qty)) {
				deckList.put(string, qty);
			}
		}
	}*/
	
	/*public void purge(String string) {
		deckList.remove(string);	
	}*/

	/*public void delete(String string, int qty) {
		if(deckList.get(string) != null) {
			int amnt = deckList.get(string);
			this.purge(string);
			deckList.put(string, amnt-qty);
		}	
	}*/
}
