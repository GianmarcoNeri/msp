package core;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class Launcher {

	public static void main(String[] args) throws IOException {
		
	        /* Set the Nimbus look and feel */
	        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
	        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
	         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
	         */
	       try {
	            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
	                if ("Windows".equals(info.getName())) {
	                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
	                    break;
	                }
	            }
	        } catch (ClassNotFoundException ex) {
	            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
	        } catch (InstantiationException ex) {
	            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
	        } catch (IllegalAccessException ex) {
	            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
	        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
	            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
	        }
	        //</editor-fold>

	        /* Create and display the form */
	        java.awt.EventQueue.invokeLater(new Runnable() {
	            public void run() {
	            	CardSearcher cs = null;
	            	boolean language = false;
	            	
	            	FileReader file = null;
					try {
						file = new FileReader("resources/lang/language.txt");
					
						BufferedReader a = new BufferedReader(file);
	            		String line;
	            		while((line = a.readLine()) != null) {
	                    	language = line.equals("1");
	            		}
	                    a.close();
	                }
	                catch (FileNotFoundException e1) {
						e1.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
					System.out.println("language: " + language);
	                
					try {
						cs = new CardSearcher(language);
					} catch (IOException e) {
						e.printStackTrace();
					}
	            	
	            	Decks decks = new Decks();

					BrowseFrame bf = new BrowseFrame(decks, cs);
					bf.setVisible(false);
	            	new MainFrame(bf, decks).setVisible(true);
	            	
	            	
	            	
	            }
	        });
	}
}
