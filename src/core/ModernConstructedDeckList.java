package core;

import java.util.HashMap;

public class ModernConstructedDeckList extends ConstructedDeckList {

	public ModernConstructedDeckList(HashMap<String, Integer> deckList) {
		super(deckList);
	}
	public ModernConstructedDeckList(HashMap<String, Integer> deckList, String name) {
		super(deckList, name);
	}
	public ModernConstructedDeckList(String name) {
		super(name);
	}
	public ModernConstructedDeckList() {
		super();
	}
	
	public boolean checkLegality(String string, int qty) {
		return ("Legal".equals(CardSearcher.allCards.get(string).getLegalities().getModern())) && (correctAmount(string, qty));
	}

}
