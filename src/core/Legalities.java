package core;

import java.lang.reflect.Field;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Legalities {
	private String _1v1;
	private String brawl;
	private String commander;
	private String duel;
	private String frontier;
	private String legacy;
	private String modern;
	private String standard;
	private String vintage;
	private String penny;
	private String pauper;
	private String future;
	
	public String getLegality(String name) {
		Field a = null;
		String b = null;
		try {
			a = Legalities.class.getDeclaredField(name);
			b = (String) a.get(this);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return b;
	}
	
	@JsonProperty("1v1")
	public String get_1v1() {
		return _1v1;
	}
	public void set_1v1(String _1v1) {
		this._1v1 = _1v1;
	}
	public String getBrawl() {
		return brawl;
	}
	public void setBrawl(String brawl) {
		this.brawl = brawl;
	}
	public String getCommander() {
		return commander;
	}
	public void setCommander(String commander) {
		this.commander = commander;
	}
	public String getDuel() {
		return duel;
	}
	public void setDuel(String duel) {
		this.duel = duel;
	}
	public String getFrontier() {
		return frontier;
	}
	public void setFrontier(String frontier) {
		this.frontier = frontier;
	}
	public String getLegacy() {
		return legacy;
	}
	public void setLegacy(String legacy) {
		this.legacy = legacy;
	}
	public String getModern() {
		return modern;
	}
	public void setModern(String modern) {
		this.modern = modern;
	}
	public String getStandard() {
		return standard;
	}
	public void setStandard(String standard) {
		this.standard = standard;
	}
	public String getVintage() {
		return vintage;
	}
	public void setVintage(String vintage) {
		this.vintage = vintage;
	}
	public String getPenny() {
		return penny;
	}
	public void setPenny(String penny) {
		this.penny = penny;
	}
	public String getPauper() {
		return pauper;
	}
	public void setPauper(String pauper) {
		this.pauper = pauper;
	}
	public String getFuture() {
		return future;
	}
	public void setFuture(String future) {
		this.future = future;
	}	
}
